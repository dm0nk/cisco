import re
from setuptools import setup

version = 1.0


with open("README", "rb") as f:
	long_descr = f.read().decode("utf-8")

setup(
	name = "Cisco Config Generator",
	packages = ["ciscoConfGen"],
	entry_points = {
		"console_scripts": ['ciscoConfGen = ciscoConfGen.ciscoConfGen:main']
		},
	version = version,
	description = "Python command line application that can generate cisco router and switch configurations",
	long_description = long_descr,
	author = "dM0nK",
	author_email = "i.deadmonkeyx@gmail.com",
	url = "http://bitbucket.org/dm0nk/cisco",
	install_requires=[
		"colorama"
	],
)


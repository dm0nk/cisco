#!/usr/bin/python
#
# Cisco Config Generator
#
# coding: UTF-8
import sys, getopt
from colorama import Fore, Back, Style

def basicConfig(configFile):
	
	# Get some settigns by asking the user
	print Fore.RED + "To create your basic config, I\'ll need some infomations !\n" + Fore.GREEN
	hostname = raw_input("Hostname [FuckingRouter01] : ") or "FuckingRouter01"
	console = raw_input("Console Password [Cisco]: ") or "Cisco"
	secret = raw_input("Enable Secret Password [Cisco1]: ") or "Cisco1"
	enable = raw_input("Enable Password [Cisco2]: ") or "Cisco2"
	vty = raw_input("Vty password [Cisco3]: ") or "Cisco3"
	banner = raw_input("Banner [Unauthorized Access]: ") or "UnAuthorized Access"
	enableSSH = raw_input("Enable ssh? [NO]: ") or "NO"
	if enableSSH == "YES":
		sshUser = raw_input("SSH Username [dmx]: ") or "dmx"
		sshPassword = raw_input("SSH Password [fuckME]: ") or "fuckME"
	
	print Fore.WHITE + "[*] Creating Configuration...\n"

	# Create config
	config = [
		"!", "enable",
		"!", "conf t",
		"!", "hostname " + hostname,
		"!", "enable password " + enable,
		"!", "enable secret " + secret,
		"!", "service password-encryption",
		"!", "line vty 0 4",
		"!", "password " + vty,
		"!", "login",
		"!", "exit",
		"!", "line console 0",
		"!", "password " + console,
		"!", "login",
		"!", "exit",
		"!", "banner motd \"" + banner + "\"",
		"!", "no ip domain-lookup",
		"!", "end",
		"!", "exit"]
	
	# open the config file for write
	with open(configFile, "w") as f:
		# Cycle array
		for line in config:
			f.write(line + "\n")
	
	print Fore.CYAN + "Config has been written to " + Fore.YELLOW + sys.argv[1] + Fore.RESET


def logo():
	print Fore.RED
	print "                                                 "
	print "   (                               (     (       "
	print "   )\    (                         )\    )\ )    "
	print " (((_)   )\   (     (     (      (((_)  (()/(    "
	print " )\___  ((_)  )\    )\    )\     )\___   /(_))_  "
	print "((/ __|  (_) ((_)  ((_)  ((_)   ((/ __| (_)) __| "
	print " | (__   | | (_-< / _|  / _ \    | (__    | (_ | "
	print "  \___|  |_| /__/ \__|  \___/     \___|    \___| "
	print "                                                 "
	print "                >> Dm0nk 2015 <<                 "
	print Fore.RESET + "\n"







def main():
	try:
		if sys.argv[1]:
			logo()
			basicConfig(sys.argv[1])
	
	except IndexError:
		logo()
		print "USAGE: " + sys.argv[0] + " <output config file>"


if __name__ == "__main__":
	main()
